function positionvetor(num,vetor)
   
   z = 0
   i = 1
   
   while i <= length(vetor)
      
      if num == vetor[i]
          
         z = i
         
      end
      
      i = i + 1
   end


   return z
end

function compareByValue(x,y)
   
   espadas =[ "2♠","3♠","4♠","5♠","6♠","7♠","8♠","9♠","10♠","J♠","Q♠","K♠","A♠"]
   copas = [ "2♥", "3♥", "4♥", "5♥", "6♥", "7♥", "8♥", "9♥","10♥", "J♥", "Q♥", "K♥", "A♥"]
   ouro = [ "2♦","3♦","4♦","5♦","6♦","7♦","8♦","9♦","10♦","J♦","Q♦","K♦","A♦"]
   paus = ["2♣","3♣","4♣","5♣","6♣","7♣","8♣","9♣","10♣","J♣","Q♣","K♣","A♣"]
   
   forcax = 0
   forcay = 0

   if x in espadas
      
      forcax = positionvetor(x,espadas)
   end

   if x in copas

      forcax = positionvetor(x,espadas)
   end
   if x in ouro

      forcax = positionvetor(x,espadas)
   end
   if x in paus

      forcax = positionvetor(x,espadas)
   end

  if y in espadas

      forcay = positionvetor(y,espadas)
  end

   if y in copas

      forcay = positionvetor(y,espadas)
   end
   if y in ouro

      forcay = positionvetor(y,espadas)
   end
   if y in paus

      forcay = positionvetor(y,espadas)
   end


   if forcay == forcax
      return 3 == 6
   end
   return forcay > forcax


end



function compareByValueAndSuit(x,y)
   
   
   forcas = [ "2♦","3♦","4♦","5♦","6♦","7♦","8♦","9♦","10♦","J♦","Q♦","K♦","A♦","2♠","3♠","4♠","5♠","6♠","7♠","8♠","9♠","10♠","J♠","Q♠","K♠","A♠","2♥", "3♥", "4♥", "5♥", "6♥", "7♥", "8♥", "9♥","10♥", "J♥", "Q♥", "K♥", "A♥","2♣","3♣","4♣","5♣","6♣","7♣","8♣","9♣","10♣","J♣","Q♣","K♣","A♣"]
   
   forcax = positionvetor(x,forcas)
   forcay = positionvetor(y,forcas)
   return forcay > forcax
end


